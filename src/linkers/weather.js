function get_weather() {
    var ps = require("python-shell")
    var path = require("path")
  
    var city = document.getElementById("city").value
    document.getElementById("city").value = "";
  
    console.log(city)
    var options = {
      scriptPath : path.join(__dirname, '/../engine/'),
      args : [city]
    }

    // ps.run('weather_engine.py',  function  (err, results)  {
    //   if  (err)  throw err;
    //   console.log('weather_engine.py finished.');
    //   console.log('results', results);
    //  });
  
    var weather = new ps.PythonShell('weather_engine.py', options);
  
    weather.on('message', function(message) {
      swal(message);
    })
  }